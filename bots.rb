require 'json'
require 'twitter_ebooks'

class CSHBot < Ebooks::Bot
  attr_accessor :model

  def top100; @top100 ||= model.keywords.take(100); end
  def top20;  @top20  ||= model.keywords.take(20); end

  def configure
    config = JSON.parse(File.read(".csh_ebooks.json"))
    puts(config)
    self.consumer_key = config["consumer_key"]
    self.consumer_secret = config["consumer_secret"]
    self.access_token = config["access_token"]
    self.access_token_secret = config["access_token_secret"]

    # Range in seconds to randomize delay when bot.delay is called
    self.delay_range = 1..6

    self.model = Ebooks::Model.load("model/csh_ebooks.model")
    @source_users = ["worr", "CSH_History", "helixoide", "eatnumber1", "harlanhaskins"]
  end

  def on_startup
    archive!
    tweet(self.model.make_statement(140))

    scheduler.every '1h' do
      tweet(self.model.make_statement(140))
    end

    scheduler.every '24h' do
      archive!
      self.model.consume_all(Dirs["corpus/*"])
      self.model.save("model/csh_ebooks.model")
    end
  end

  def can_follow?(user)
    @source_users.reduce(0) { |result, su| result + (twitter.friendship?(user, su) ? 1 : 0) } > 1
  end

  def on_message(dm)
    reply(dm, self.model.make_response(dm.full_text, 130))
  end

  def on_follow(user)
    if self.can_follow?(user.screen_name)
      follow(user.screen_name)
    end
  end

  def on_mention(tweet)
    if self.can_follow?(tweet.user.screen_name)
      follow(tweet.user.screen_name)
    end

    reply(tweet, self.model.make_response(meta(tweet).mentionless, meta(tweet).limit))
  end

  def on_timeline(tweet)
    return if tweet.retweeted_status?

    tokens = Ebooks::NLP.tokenize(tweet.text)
    interesting = tokens.find { |t| top100.include?(t.downcase) }
    very_interesting = tokens.find_all { |t| top20.include?(t.downcase) }.length > 2

    if very_interesting
      if rand < 0.5
        favorite(tweet)
      elsif rand < 0.01
        reply(tweet, self.model.make_response(meta(tweet).mentionless, meta(tweet).limit))
      end
    elsif interesting
      if rand < 0.01
        favorite(tweet)
      end
    end
  end

  def on_favorite(user, tweet)
    if self.can_follow?(user.screen_name)
      follow(user.screen_name)
    end
  end

  def archive!()
    archived = []
    twitter.friends.each do |friend|
      log "Archiving #{friend.screen_name}"
      archive = Ebooks::Archive.new(friend.screen_name, nil, twitter)
      archive.sync
      archived.push(friend.screen_name)
    end

    Dir["corpus/*"].each do |file|
      friend = File.basename(file, ".*")
      if archived.include?(friend)
        archive = Ebooks::Archive.new(friend, nil, twitter)
        archive.sync
        archived.push(friend)
      end
    end
  end
end

# Make a CSHBot and attach it to an account
CSHBot.new("csh_ebooks") do |bot|
end
